#!/bin/bash
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes
if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

alias terminus=/home/amccoy/terminus/vendor/bin/terminus

function generate_login(){
        arg1=$1
        terminus remote:drush $arg1 -- uli;
}

alias shinde='rm -r'

alias sites='cd /opt/sites/drupal-dev.itos.uga.edu/pantheon/'

function backup_pantheon_sites(){
        PANTHEON_SITES="$(terminus site:list --format=list --field=name)"
        for PANTHEON_SITE_NAME in $PANTHEON_SITES
        do
                echo $PANTHEON_SITE_NAME
                # Check if the site is frozen
                IS_FROZEN="$(terminus site:info $PANTHEON_SITE_NAME --field=frozen)"

                # If the site is frozen
                if [[ "1" = "${IS_FROZEN}" ]]
                then
                        # Then skip it
                        echo -e "Skipping a backup of the site '$PANTHEON_SITE_NAME' because it is frozen...\n"
                else
                        if [[ "ctsa-empact" = "$PANTHEON_SITE_NAME" ]]
                        then
                                #skip it, we don't own that one
                                echo "CTSA-EMPACT IS NOT OURS DON'T TOUCH IT"
                        else
                                # Otherwise create a backup of the live environment of the site
                                echo -e "Creating a backup of the dev environment for the site '$PANTHEON_SITE_NAME'...\n"
                                #terminus backup:create $PANTHEON_SITE_NAME.live
                                #terminus backup:create $PANTHEON_SITE_NAME.dev
                        fi
                        GIT_URL="http://${ID}@codeserver.dev.${ID}.drush.in:2222/~/repository.git"
                fi
        done
}

terminus_auth() {
	response=`terminus auth:whoami`
	if [ "$response" = "" ]; then
		echo "You are not authenticated with Terminus..."
		terminus auth:login
		if [ $? -eq 0 ]; then
    		        echo "Login successful!"
		else
    			echo "Login failed. Please re-run the script and try again."
			exit 0
		fi
	else
		read -p "$response.  [y]Continue or [n]login as someone else? [y/n] " login;
		case $login in
			[Yy]* ) ;;
			[Nn]* ) terminus auth:logout;
					terminus auth:login;;
		esac
	fi
}

step_route() {
    if [ "$SITENAME" = 'all' ]; then
        PANTHEON_SITES="$(terminus site:list --format=list --field=name)"
        for PANTHEON_SITE_NAME in $PANTHEON_SITES
        do
          git_drupal_update $PANTHEON_SITE_NAME;
          git_finish $PANTHEON_SITE_NAME;
        done
    else
      PANTHEON_SITE_NAME=$SITENAME
      echo $PANTHEON_SITE_NAME
      terminus site:info $PANTHEON_SITE_NAME
      echo "$FRAMEWORK"
      if [ "$FRAMEWORK" = 'framework: drupal' ]; then
        	    echo "Updating $PANTHEON_SITE_NAME site with git..."
              case $STEP in
                      [start]* ) git_drupal_update $PANTHEON_SITE_NAME;;
                      [finish]* ) git_finish $PANTHEON_SITE_NAME;;
                      * ) echo "not a valid function."; exit 1;;
              esac
      fi

      if [ "$FRAMEWORK" = 'framework: wordpress' ]; then
              case $STEP in
                      [start]* ) git_wordpress_update $SITENAME;;
                      [finish]* ) git_finish $SITENAME;;
                      * ) echo "not a valid function."; exit 1;;
              esac
      fi
    fi
}

git_update_prep() {
	echo "Updating ${PANTHEON_SITE_NAME} site with git..."
	MDENV='auto-update'
	read -p "Backup dev? [y/n]  " yn
	case $yn in
		[Yy]* ) echo "Creating backup of prod environment for ${PANTHEON_SITE_NAME}... ";
				terminus backup:create $PANTHEON_SITE_NAME.dev;;
	esac
	if [ $? = 1 ]; then
		$((ERRORS++))
		echo "error in backup dev"
	fi

	envExist=`terminus env:list $PANTHEON_SITE_NAME | grep "${MDENV}"`

	if [ -z "$envExist" ]; then
		echo "Creating git auto-update environment..."
		read -p "Pull down db from which environment? (dev/test/live) "	FROMENV
		terminus git:create ${PANTHEON_SITE_NAME}.dev ${MDENV}
		if [ $? = 1 ]; then
			$((ERRORS++))
			echo "error in creating env"
		fi
	else
		read -p "Multidev auto-update environment already exists.  Deploy db from which environment? (dev/test/live/none) " FROMENV
		if [ $FROMENV != 'none' ]; then
			terminus env:deploy $PANTHEON_SITE_NAME.dev
		fi
	fi

	echo "The URL for the new environment is https://${MDENV}-${PANTHEON_SITE_NAME}.pantheonsite.io/"

	echo "Switching to git connection-mode..."
  terminus connection:set $PANTHEON_SITE_NAME.dev git
  GIT_COMMAND="$(terminus connection:info $PANTHEON_SITE_NAME.dev --field git_command)"
  eval "$GIT_COMMAND"
  cd $PANTHEON_SITE_NAME
	if [ $? = 1 ]; then
		$((ERRORS++))
		echo "error in switching to sftp"
	fi

}

git_update_errors() {
	if [ $ERRORS != '0' ]; then
		WORD='error was'
		if [ $ERRORS > '1' ]; then
			WORD='errors were'
		fi
		echo "$ERRORS $WORD reported.  Review this deploy."
	fi
}

git_drupal_update() {
  #prepare site to update and move to cloned directory
	git_update_prep
	#run composer updates or drush updates
	composer update
	#run drush updb
  drush updb
  #run drush updates
	if [ $? = 1 ]; then
		$((ERRORS++))
		echo "error in drush up"
		UPFAIL='Drush up failed.'
	fi
	if [ -z "$UPFAIL" ]; then
		echo "Running 'drush updb'..."
		terminus drush updb -y --env=${MDENV} --site=${PANTHEON_SITE_NAME}
		if [ $? = 1 ]; then
			$((ERRORS++))
			echo "error in updb"
			UPDBFAIL='Drush updb failed.'
		fi
		echo "Site's modules have been updated on $MDENV git site. Please test it here: http://${MDENV}-${PANTHEON_SITE_NAME}.pantheon.io/"
	fi
	git_update_errors
}

git_merge() {
	## In this case, 'origin' is Pantheon remote name.
  git clone $GITURL clone-${PANTHEON_SITE_NAME}
  cd clone-${PANTHEON_SITE_NAME}
  git fetch --all
  if [ $? -ne 0 ]; then
    echo "git fetch --all failed"
    exit 1
  fi
	git checkout $MDENV
  git pull origin $MDENV
  git checkout master
  git pull origin master
  git merge $MDENV
	if [ $? -ne 0 ]; then
    echo "Merge failed"
    Timestamp = date +"%Y-%m-%d-%T"
    git merge --abort 2> conflicts.${Timestamp}.txt
    git reset --hard origin/master
    git clean -df
    exit 1
	fi

	git push origin master
	echo "Pushed to master. Please visit the dev environment to view your updates"
}

git_deploy_to_test() {
	read -p "Deploy changes to test environment on Pantheon? [y/n] " DEPLOYTEST
	case $DEPLOYTEST in
		[Yy]* ) read -p "Please provide a note to attach to this deployment to Test: " MESSAGE
						terminus env:deploy ${PANTHEON_SITE_NAME}.dev;;
		[Nn]* ) exit 0;;
	esac
}

git_deploy_to_live() {
	read -p "Deploy changes to live environment on Pantheon? [y/n] " DEPLOYLIVE
	case $DEPLOYLIVE in
		[Yy]* ) read -p "Please provide a note to attach to this deployment to Live: " MESSAGE
						terminus env:deploy ${PANTHEON_SITE_NAME}.dev;;
		[Nn]* ) exit 0;;
	esac
}

git_finish() {
	SITE=$1
	MDENV='auto-update'
	SITEINFO=`terminus site:info --site=${PANTHEON_SITE_NAME} --field=id`
	SITEID=${SITEINFO#*: }
	GITURL="ssh://codeserver.dev.${SITEID}@codeserver.dev.${SITEID}.drush.in:2222/~/repository.git"

    read -p "Please provide git commit message: " MESSAGE
        terminus site:code commit --site=${PANTHEON_SITE_NAME} --env=${MDENV} --message="$MESSAGE"
        if [ $? -ne 0 ]; then
    echo "git commit failed"
    exit 1
        fi

    echo "Returning auto-update to git connection-mode..."
    terminus connection:set $PANTHEON_SITE_NAME.dev sftp
    if [ $? -ne 0 ]; then
    echo "Switching connection mode back to git failed."
    exit 1
    fi

	git_merge


	read -p "Delete auto-update environment? [y/n]  " yn
	case $yn in
		[Yy]* ) terminus site:delete-env --site=${PANTHEON_SITE_NAME} --env=${MDENV}
	esac

	git_deploy_to_test

	git_deploy_to_live

}

git_wordpress_update() {
	MDENV='auto-update'
	SITE=$1

	git_update_prep
	terminus wp plugin update --all=1 --site=$PANTHEON_SITE_NAME --env=$MDENV
	if [ $? = 1 ]; then
		$((ERRORS++))
		echo "error in wp plugin update"
		UPFAIL='WP up failed.'
	else
		echo "Site's plugins have been updated on $MDENV git site. Please test it here: http://${MDENV}-${SITENAME}.pantheon.io/"
	fi


}

git_update(){
  terminus_auth
  read -p "Load site list? [y/n]  " loadlist
  	case $loadlist in
  		[Yy]* ) echo 'Loading site list (this can take a few minutes...)';terminus site:list;;
  	esac

  read -p 'Type in site name (or type "all") and press [Enter] to start updating: ' $SITENAME
  STEP='start'
  step_route

  read -p "Press [Enter] to finish updating ${SITENAME}"
  STEP='finish'
  step_route

  read -p 'Log out of Terminus? [y/n] ' LOGOUT
    case $LOGOUT in
          [Yy]* ) terminus auth:logout

    esac

  read -p "Delete pantheon-clone folder? [y/n] " yn
    case $yn in
          [Yy]*) cd ..
                 echo "deleting pantheon-clone..."
                 rm -rf pantheon-clone_${SITENAME};;
    esac

  exit 0
}
